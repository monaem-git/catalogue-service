package org.opendevup;

import org.opendevup.dao.CategoryRepository;
import org.opendevup.dao.ProductRepository;
import org.opendevup.entities.Category;
import org.opendevup.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.stream.Stream;

@SpringBootApplication
public class CatalogueServiceApplication implements CommandLineRunner {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    public static void main(String[] args) {

        SpringApplication.run(CatalogueServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        categoryRepository.deleteAll();
        productRepository.deleteAll();

        Stream.of("C1 Ordinateurs","C2 Imprimentes").forEach(c->{
            categoryRepository.save(new Category(c.split(" ")[0],c.split(" ")[1],new ArrayList<>()));
        });

        Category c1 = categoryRepository.findById("C1").orElse(null);
        Stream.of("P1", "P2", "P3", "P4").forEach(name->{
            Product p1 = productRepository.save(new Product(null,name, Math.random()*50,c1));
            c1.getProducts().add(p1);
            categoryRepository.save(c1);
        });

        Category c2 = categoryRepository.findById("C2").orElse(null);
        Stream.of("P5", "P6", "P7").forEach(name->{
            Product p2 = productRepository.save(new Product(null,name, Math.random()*50,c2));
            c2.getProducts().add(p2);
            categoryRepository.save(c2);
        });

       categoryRepository.findAll().forEach( c-> {
            System.out.println(c.toString());
        });
        productRepository.findAll().forEach( p-> {
            System.out.println(p.toString());
        });

    }
}

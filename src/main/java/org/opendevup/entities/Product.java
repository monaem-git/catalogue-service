package org.opendevup.entities;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor @AllArgsConstructor @ToString
@Document
public class Product {
    private String id;
    private String name;
    private double price;
    @DBRef
    private Category category;

}
